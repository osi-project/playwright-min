FROM ubuntu:latest

COPY ./app/ /usr/src/app/
WORKDIR /usr/src/app
ARG DEBIAN_FRONTEND=noninteractive

# Install node16
# RUN apt-get update && apt-get install -y apt-utils libterm-readline-gnu-perl curl && \
#    curl -sL https://deb.nodesource.com/setup_16.x | bash - && \
#    apt-get install -y nodejs

RUN apt-get update && apt-get -y install curl && \
    curl -s https://deb.nodesource.com/setup_18.x | bash && \
    # apt-get update && \             
    # apt-get remove -y nodejs-legacy && \
    apt-get install -y nodejs
      

# Install Fonts
# RUN apt-get install fonts-

# Install chromium
#RUN apt update && apt install -y chromium-browser

#RUN touch package.json && \
#    echo '{"name": "root","description":"","license":"ISC"}' > package.json 

RUN npm init -y && \
    npm install
    
    # 2024 deprecaded   
    # PLAYWRIGHT_SKIP_BROWSER_DOWNLOAD=1 npm i  -D playwright && \
    # npm i -D playwright-chromium && \
    # npm i -D playwright-firefox && \
    # npm i -D @playwright/test && \
    # npm i -D reg-cli

RUN npx playwright install --with-deps
# RUN npx playwright install-deps

RUN npm install -D @playwright/test@latest && \
    npx playwright --version

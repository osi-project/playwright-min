const { test, expect } = require("@playwright/test");

const playwright = require("playwright");

const chromium = require("playwright").chromium;
const firefox = require("playwright").firefox;
//const webkit = require("playwright").webkit;
//const devices = require("playwright").devices;

test("basic test", async ({ page }) => {
  await page.goto("https://playwright.dev/");
  const title = page.locator(".navbar__inner .navbar__title");
  await expect(title).toHaveText("Playwright");
});
